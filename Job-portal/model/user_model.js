const mongoose = require('mongoose');
const bcrypt = require('bcrypt');

const Schema = mongoose.Schema;

const UserSchema = new Schema({
  username: {
    type: String,
    required: true,
    unique: true
  },
  password: {
    type: String,
    required: true
  }
});

// pre-hook - will save the text password as hash value in database
UserSchema.pre(
    'save',
    async function(next) {
      const user = this;
      const hash = await bcrypt.hash(this.password, 10);
  
      this.password = hash;
      next();
    }
  );


// Validate the password entered by the user
UserSchema.methods.isValidPassword = async function(password) {
    const user = this;
    const isValid = await bcrypt.compare(password, user.password);
    return isValid;
  }
  

const UserModel = mongoose.model('user', UserSchema);

module.exports = UserModel;