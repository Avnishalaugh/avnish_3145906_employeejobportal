var express = require('express');
var router = express.Router();
var openingsModel = require('../model/openings');
var openings = openingsModel.find({});

/* GET home page. */
router.get('/', function (req, res, next) {
  openings.exec(function (err, data) {
    if (err) throw err;
    res.render('pages/index', { openings: data });
  });
});

router.get('/home', function (req, res, next) {
  openings.exec(function (err, data) {
    if (err) throw err;
    res.render('pages/home', { openings: data });
  });
});


/* Manager APIs*/
router.get('/openings/create', function (req, res, next) {
  res.render('pages/createOpenings');
});


router.get('/openings/all', function (req, res, next) {
  openings.exec(function (err, data) {
    console.log(data);
     if (err) throw err;
     res.render('pages/allOpenings', { openings: data });
   });
});


router.get('/login', function (req, res, next) {
  openings.exec(function(err,data) {
  if(err) throw err;
  res.render('pages/login');
  });
  });

router.get('/register', function (req, res, next) {
  openings.exec(function (err, data) {
    if (err) throw err;
    res.render('pages/register');
  });
});


router.post('/openings/create', function (req, res, next) {
  const data = req.body;
  console.log(data);
  var openingsData = new openingsModel({
    projectName: data.projectName,
    clientName: data.clientName,
    roleName: data.roleName,
    jobDescription: data.jobDescription,
    technologyStack: data.technologyStack,
    createdBy: data.createdBy
  });
  console.log(openingsData);
  openingsData.save();
  openings.exec(function (err, data) {
    if (err) throw err;
    res.render('pages/allOpenings', { openings: data });
  });
});


router.get('/openings/update/:id', function (req, res, next) {
  console.log("id: " + req.params['id']);
  var id = req.params['id'];
  var jobOpeningById = openingsModel.find({ "_id": id });
  jobOpeningById.exec(function (err, jobOpeningData) {
    if (err) throw err;
    console.log("get data to be updated: " + JSON.stringify(jobOpeningData[0]));
    res.render('pages/updateOpenings', { data: jobOpeningData[0] });
  });

});

router.post('/openings/update', function (req, res, next) {
  console.log("update data: ");
  const data = req.body;
  console.log(data);
  const _id = data.id;
  console.log(_id);

  if (_id.match(/^[0-9a-fA-F]{24}$/)) {
  openingsModel.findByIdAndUpdate({ _id }, {
  "projectName": data.projectName,
  "clientName": data.clientName,
  "roleName": data.roleName,
  "jobDescription": data.jobDescription,
  "technologyStack": data.technologyStack,
  "status": data.status,
  "createdBy": data.createdBy
  }, function (err, result) {
  if (err) throw err;
  
  openings.exec(function (err, data) {
  if (err) throw err;
  res.render('pages/allOpenings', { title: 'Job Portal', openings: data });
  });
  });
  }
});

module.exports = router;
